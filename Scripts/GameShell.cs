﻿using System;
using DG.Tweening;
using GameShell.Buttons;
using GameShell.Core;
using GameShell.Core.EventSystem;
using GameShell.Dialogs;
using GameShell.Screens;
using UnityEngine;

namespace GameShell
{
    public enum Scene
    {
        None,
        Preload,
        MainMenu,
        Game
    }


    

    public class GameShell : MonoBehaviour {
        
        public EventSystem EventSystem { get; private set; }
        public LocalizationSystem Localization { get; private set; }

        [HideInInspector]public GameObject RootUgui { get; private set; }
        [HideInInspector]public GameObject DialogsContainer { get; private set; }
        [HideInInspector]public GameObject ScreensContainer { get; private set; }

        public DialogBlocker Blocker { get; private set; }
        public SettingsGameShell Settings { get; private set; }

        public Action CallbackUpdate;

        public static GameShell Instance {
            get { return _instance ?? (_instance = new GameShell()); }
        }

        private SplashScreen _splashScreen;
        private MainMenuScreen _mainMenuScreen;
        private ControlPanel _controlPanel;
        private static GameShell _instance;

        public Scene CurrentScene
        {
            get { return StateMachine.CurrentState; }
        }

        protected readonly StateMachine<Scene> StateMachine = new StateMachine<Scene>();

        private GameShell()
        {
            _instance = this;
        }

        void Start ()
        {
            if (Instance != null)
            {
                DOTween.Init(false, false, LogBehaviour.ErrorsOnly);

                Settings = Resources.Load<SettingsGameShell>("GameShell/Data/SettingsGameShell");

                RootUgui = GameObject.Find("UGUI");

                if (RootUgui == null)
                {
                    var tempGo = Resources.Load<GameObject>("GameShell/UGUI");
                    RootUgui = Instantiate(tempGo);
                    RootUgui.name = "UGUI";
                }

                ScreensContainer = RootUgui.transform.FindChild("Screens").gameObject;
                DialogsContainer = RootUgui.transform.FindChild("Dialogs").gameObject;

                EventSystem = new EventSystem();
                Localization = new LocalizationSystem(Settings.PathToLocalizationFile, EventSystem, Settings.EnableLocalization);
                Blocker = new DialogBlocker(DialogsContainer, Settings.BlockerDialog);

                Init();
            }
        }

        private void Init()
        {
            StateMachine.Add(Scene.Preload, OnPreload);
            StateMachine.Add(Scene.MainMenu, OnMainMenu, OnHideMainMenu);
            StateMachine.Add(Scene.Game, OnGame, OnHideGame);
            StateMachine.SetState(Scene.None);

            _splashScreen = new SplashScreen(ScreensContainer, Settings.SplashScreen, Settings.TimeSplashComplete);
            
            _mainMenuScreen = new MainMenuScreen(ScreensContainer, Settings.MainMenu, () => {StateMachine.SetState(Scene.Game);});
            _mainMenuScreen.Init();
            _mainMenuScreen.Hide();

            _controlPanel = new ControlPanel(ScreensContainer, Settings.ControlPanel, () => { StateMachine.SetState(Scene.MainMenu); });
            _controlPanel.Init();
            _controlPanel.Hide();

            EventSystem.Call(GameShellEvents.GameShellInit);
            StateMachine.SetState(Scene.Preload);
        }

        private void OnPreload(object obj)
        {
            _splashScreen.Start(CompletePreload);
            EventSystem.Call(GameShellEvents.StartShowSplash);
        }

        private void CompletePreload()
        {
            _splashScreen.Destroy();
            _splashScreen = null;

            EventSystem.Call(GameShellEvents.CompleteShowSplash);

            StateMachine.SetState(Scene.MainMenu);
        }

        private void OnMainMenu(object obj)
        {
            _mainMenuScreen.Show();
        }

        private void OnHideMainMenu()
        {
            _mainMenuScreen.Hide();
        }

        private void OnGame(object obj)
        {
            _controlPanel.Show();
        }

        private void OnHideGame()
        {
            _controlPanel.Hide();
        }

        public void Destroy()
        {
            EventSystem.Call(GameShellEvents.GameShellDestroy);
        }

        public void ShowMenuScreen()
        {
            StateMachine.SetState(Scene.MainMenu);
        }

        public void ShowGameControlPanel()
        {
            StateMachine.SetState(Scene.Game);
        }

        public void ShowSettingsDialog()
        {
            var settingsDialog = new SettingsDialog(DialogsContainer, Settings.SettingsDialog);
            settingsDialog.Init();
        }

        public void ShowParentControlDialog(Action succesAction)
        {
            var parentControlDialog = new ParentControlDialog(DialogsContainer, Settings.ParentControlDialog, succesAction);
            parentControlDialog.Init();
        }

        public void ShowBackArrowButton()
        {
            EventSystem.Call(GameShellEvents.ShowArrowButton, new ArrowButtonParams(ArrowButtonType.Back));
        }

        public void ShowForwardArrowButton()
        {
            EventSystem.Call(GameShellEvents.ShowArrowButton, new ArrowButtonParams(ArrowButtonType.Forward));
        }

        public void HideBackArrowButton()
        {
            EventSystem.Call(GameShellEvents.HideArrowButton, new ArrowButtonParams(ArrowButtonType.Back));
        }

        public void HideForwardArrowButton()
        {
            EventSystem.Call(GameShellEvents.HideArrowButton, new ArrowButtonParams(ArrowButtonType.Forward));
        }

        private void Update ()
        {
            if(_splashScreen != null)
                _splashScreen.Update();

            if (CallbackUpdate != null)
                CallbackUpdate();
        }
    }
}