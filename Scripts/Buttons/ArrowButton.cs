﻿using System;
using GameShell.Core.EventSystem;
using UnityEngine;

namespace GameShell.Buttons
{
    public enum ArrowButtonType
    {
        Back,
        Forward
    }

    public class ArrowButton : DoubleClickButton
    {
        public readonly ArrowButtonType Type;
        private bool _hided;

        public ArrowButton(ArrowButtonType type, GameObject parent, GameObject view, Action onClickAction, float time):base(parent, view, onClickAction, time)
        {
            Type = type;
        }

        protected override void OnInit()
        {
            base.OnInit();

            GameShell.Instance.EventSystem.Attach(GameShellEvents.ShowArrowButton, OnShowArrow);
            GameShell.Instance.EventSystem.Attach(GameShellEvents.HideArrowButton, OnHideArrow);
        }

        protected override void FirstClick()
        {
            GameShell.Instance.StopCoroutine(TimerCoroutine);
            Animator.ResetTrigger(AnimateDeactivate);

            Animator.SetTrigger(AnimateActivate);

            TimerCoroutine = GameShell.Instance.StartCoroutine(Timer.Start(TargetTime, CompleteTimer));
            GameShell.Instance.CallbackUpdate += TapNotOnButton;

          //  Debug.Log("FirstClick");
        }

        protected override void SecondClick()
        {
            GameShell.Instance.StopCoroutine(TimerCoroutine);

            TimerCoroutine = GameShell.Instance.StartCoroutine(Timer.Start(TargetTime, CompleteTimer));

          //  Debug.Log("SecondClick");
        }

        private void OnShowArrow(EventParams param)
        {
            var arrowParams = (ArrowButtonParams) param;
            if (arrowParams != null && arrowParams.Type == Type)
            {
                _hided = false;
                Show();
            }
        }

        private void OnHideArrow(EventParams param)
        {
            var arrowParams = (ArrowButtonParams)param;
            if (arrowParams != null && arrowParams.Type == Type)
            {
                Hide();
                _hided = true;
            }
        }

        public override void Show()
        {
            if(_hided)return;

            base.Show();

             TimerCoroutine = GameShell.Instance.StartCoroutine(Timer.Start(TargetTime, HideArrow));

            Animator.ResetTrigger(AnimateDeactivate);

          //  Debug.Log("show");
        }

        public override void Hide()
        {
            if (_hided) return;

           // Debug.Log("Hide");
            base.Hide();
        }

        private void HideArrow()
        {
            Animator.ResetTrigger(AnimateActivate);
            Animator.SetTrigger(AnimateDeactivate);
           // Debug.Log("deactivate");
        }

        protected override void OnDestroy()
        {
            GameShell.Instance.EventSystem.Detach(GameShellEvents.ShowArrowButton, OnShowArrow);
            GameShell.Instance.EventSystem.Detach(GameShellEvents.HideArrowButton, OnHideArrow);

            base.OnDestroy();
        }
    }
}
