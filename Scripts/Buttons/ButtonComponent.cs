﻿using System;
using GameShell.Core;
using UnityEngine;
using UnityEngine.UI;

namespace GameShell.Buttons
{
    public class ButtonComponent : GuiComponent
    {
        protected Action OnClickAction;
        protected Button Button;

        public ButtonComponent(GameObject parent, GameObject view, Action onClickAction):base(parent, view)
        {
            OnClickAction = onClickAction;
        }

        protected override void OnInit()
        {
            Button = View.GetComponent<Button>();
            Button.onClick.AddListener(OnClick); 
        }

        protected virtual void OnClick()
        {
            if (OnClickAction != null)
                OnClickAction();
        }

        protected override void OnDestroy()
        {
            Button.onClick.RemoveListener(OnClick);
            Button = null;
            OnClickAction = null;
        }

        protected override void OnUpdate()
        {
        }
    }
}
