﻿using System;
using GameShell.Buttons;
using GameShell.Core;
using GameShell.Core.Extensions;
using GameShell.Dialogs;
using UnityEngine;
using Object = UnityEngine.Object;

namespace GameShell.Screens
{
    public class MainMenuScreen:GuiComponent
    {
        private Action _action;
        private ButtonComponent _buttonPlay;
        private ButtonComponent _buttonSettings;
        private ButtonComponent _buttonHelp;
        private ButtonComponent _buttonAd;

        public MainMenuScreen(GameObject parent, GameObject view, Action action):base(parent, view)
        {
            _action = action;
        }

        protected override void OnInit()
        {
            var settings = GameShell.Instance.Settings;

            var background = Object.Instantiate(settings.BackgroundMenu);
            var backAnchor = View.transform.FindChild("AnchorBackground").gameObject;
            backAnchor.AddChildUI(background);
            
            _buttonPlay = new ButtonComponent(View, settings.ButtonPlay, OnClickPlay);
            _buttonSettings = new ButtonComponent(View, settings.ButtonSettings, OnClickSettings);
            _buttonHelp = new ButtonComponent(View, settings.ButtonHelp, OnClickHelp);
            _buttonAd = new ButtonComponent(View, settings.ButtonAd, OnClickAd);

            _buttonPlay.Init();
            _buttonSettings.Init();
            _buttonHelp.Init();
            _buttonAd.Init();
        }

        private void OnClickPlay()
        {
            if (_action != null)
                _action();
        }

        private void OnClickSettings()
        {
            GameShell.Instance.ShowSettingsDialog();
        }

        private void OnClickHelp()
        {
            GameShell.Instance.ShowParentControlDialog(ShowHelpDialog);
        }

        private void ShowHelpDialog()
        {
            var helpDialog = new HelpDialog(GameShell.Instance.DialogsContainer, GameShell.Instance.Settings.HelpDialog);
            helpDialog.Init();
        }

        private void OnClickAd()
        {
            Debug.Log("OnClickAd click");
        }

        public override void Show()
        {
            base.Show();
            GameShell.Instance.EventSystem.Call(GameShellEvents.ShowMainMenu);
        }

        public override void Hide()
        {
            base.Hide();
            GameShell.Instance.EventSystem.Call(GameShellEvents.HideMainMenu);
        }

        protected override void OnDestroy()
        {
            _buttonPlay.Destroy();
            _buttonSettings.Destroy();
            _buttonHelp.Destroy();
            _buttonAd.Destroy();
        }

        protected override void OnUpdate()
        {
        }
    }
}