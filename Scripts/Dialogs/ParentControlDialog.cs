﻿using System;
using DG.Tweening;
using GameShell.Core.Extensions;
using GameShell.Helpers;
using UnityEngine;
using Random = UnityEngine.Random;

namespace GameShell.Dialogs
{
    public class ParentControlDialog : Dialog
    {
        private string _targetNumbers = "";
        private int _clickCounter;
        private Action _succesAction;
        private ParentControlHelper _parentControlHelper;

        public ParentControlDialog(GameObject parent, GameObject view, Action succesAction):base(parent, view)
        {
            _succesAction = succesAction;
        }

        protected override void OnInit()
        {
            base.OnInit();

            _parentControlHelper = View.GetComponent<ParentControlHelper>();

            if (GameShell.Instance.Settings.EnableLocalization)
            {
                _parentControlHelper.HeaderText.text = "header_pc".ToLocalization();
                _parentControlHelper.DescriptionText.text = "description_pc".ToLocalization();
            }

            _parentControlHelper.TargetNumberText.text = GetNewTargetNumbers();
            _parentControlHelper.InputText.text = "";

            foreach (var buttonItem in _parentControlHelper.Buttons)
            {
                var number = buttonItem.Number;
                buttonItem.Button.onClick.AddListener(() => ClickNumber(number));
            }

            _clickCounter = 0;

            GameShell.Instance.EventSystem.Call(GameShellEvents.ShowParentControlDialog);
        }

        private void ClickNumber(int numder)
        {
            _clickCounter++;

            _parentControlHelper.InputText.text += numder;

            if (_clickCounter != 3) return;

            if (_parentControlHelper.InputText.text == _targetNumbers)
            {
                if (_succesAction != null)
                    _succesAction();

                Destroy();
            }
            else
            {
                _clickCounter = 0;
                _parentControlHelper.InputText.text = "";
                _parentControlHelper.TargetNumberText.text = GetNewTargetNumbers();

                RectTransform.DOPunchAnchorPos(new Vector2(30, 0), 0.5f);
            }
        }

        private string GetNewTargetNumbers()
        {
            var textNumbers = "";
            _targetNumbers = "";
            for (var i = 0; i < 3; i++)
            {
                var numder = Random.Range(0, 9);
                _targetNumbers += numder;

                textNumbers += numder.ToString().ToLocalization();
                if (i != 2)
                    textNumbers += ", ";
            }

            return textNumbers;
        }

        protected override void OnDestroy()
        {
            base.OnDestroy();

            foreach (var buttonItem in _parentControlHelper.Buttons)
            {
                buttonItem.Button.onClick.RemoveAllListeners();
            }
            _succesAction = null;
            _parentControlHelper = null;

            GameShell.Instance.EventSystem.Call(GameShellEvents.HideParentControlDialog);
        }
    }
}