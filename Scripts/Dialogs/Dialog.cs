﻿using DG.Tweening;
using GameShell.Buttons;
using GameShell.Core;
using UnityEngine;

namespace GameShell.Dialogs
{
    public class Dialog : GuiComponent
    {
        protected DoubleClickButton ButtonClose;

        public Dialog(GameObject parent, GameObject view):base(parent, view)
        {
        }

        protected override void OnInit()
        {
            var settings = GameShell.Instance.Settings;

            var anchor = View.transform.FindChild("AnchorBtnClose").gameObject;
            ButtonClose = new DoubleClickButton(anchor, settings.ButtonClose, OnClickClose, settings.TimeDeactivationButtons);
            ButtonClose.Init();

            RectTransform.localScale = Vector3.zero;
           
            AnimationShow();
            GameShell.Instance.Blocker.Show();
        }

        private void AnimationShow()
        {
            RectTransform.DOScale(Vector3.one, 0.2f);
        }

        private void AnimationHide()
        {
            RectTransform.DOScale(Vector3.zero, 0.2f).OnComplete(OnComplete);
        }

        private void OnClickClose()
        {
            GameShell.Instance.Blocker.Hide();
            AnimationHide();
        }

        private void OnComplete()
        {
            Destroy();
        }

        protected override void OnDestroy()
        {
            ButtonClose.Destroy();
        }

        protected override void OnUpdate()
        {
        }
    }
}