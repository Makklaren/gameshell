﻿using GameShell.Buttons;
using GameShell.Core.EventSystem;

namespace GameShell
{
    public class GameShellEvents
    {
        public static Event StartShowSplash = new Event(WrapTag("StartShowSplash"));
        public static Event CompleteShowSplash = new Event(WrapTag("CompleteShowSplash"));

        public static Event ShowMainMenu = new Event(WrapTag("ShowMainMenu"));
        public static Event HideMainMenu = new Event(WrapTag("HideMainMenu"));

        public static Event ShowControlPanel = new Event(WrapTag("ShowControlPanel"));
        public static Event HideControlPanel = new Event(WrapTag("HideControlPanel"));

        public static Event ShowSettingsDialog = new Event(WrapTag("ShowSettingsDialog"));
        public static Event HideSettingsDialog = new Event(WrapTag("HideSettingsDialog"));

        public static Event ShowParentControlDialog = new Event(WrapTag("ShowParentControlDialog"));
        public static Event HideParentControlDialog = new Event(WrapTag("HideParentControlDialog"));

        public static Event GameShellInit = new Event(WrapTag("GameShellInit"));
        public static Event GameShellDestroy = new Event(WrapTag("GameShellDestroy"));

        public static Event HideArrowButton = new Event(WrapTag("HideArrowButton"));
        public static Event ShowArrowButton = new Event(WrapTag("ShowArrowButton"));
     
        public static Event ClickBackArrowButton = new Event(WrapTag("ClickBackArrowButton"));
        public static Event ClickForwardArrowButton = new Event(WrapTag("ClickForwardArrowButton"));

        public static Event ChangeLangauge = new Event(WrapTag("ChangeLangauge"));

        private static string WrapTag(string nameEvent)
        {
            return typeof(GameShellEvents).Name + "." + nameEvent;
        }
    }

    public class ArrowButtonParams : EventParams
    {
        public ArrowButtonType Type;

        public ArrowButtonParams(ArrowButtonType type)
        {
            Type = type;
        }
    }

    public class LangaugeParams : EventParams
    {
        public string NemLocalId;

        public LangaugeParams(string localId)
        {
            NemLocalId = localId;
        }
    }
}
