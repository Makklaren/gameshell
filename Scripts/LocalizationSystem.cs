﻿using System;
using System.Collections.Generic;
using GameShell.Core.EventSystem;
using UnityEngine;

namespace GameShell
{
    public class LocalizationSystem
    {
        private readonly EventSystem _eventSystem;
        public string CurrentLocal;

        private Dictionary<string, Dictionary<string, string>> _local;
        private Dictionary<string, string> _currentLocalDictionary;

        public LocalizationSystem(string pathCvs, EventSystem eventSystem, bool enable)
        {
            _eventSystem = eventSystem;

            if (enable)
            {
                TextAsset text = Resources.Load(pathCvs) as TextAsset;

                if (text != null)
                    ParseCsvFile(text.text);
                else
                    throw new Exception("Not load csv file!");

                SetLangauge(Application.systemLanguage.ToString());
            }
            else
            {
                CurrentLocal = Application.systemLanguage.ToString();
            }
        }

        private void SetLangauge(string local)
        {
            if (_local == null)
            {
                CurrentLocal = local;
                return;
            }

#if UNITY_ANDROID || UNITY_IOS
            if (!_local.ContainsKey(local))
                local = "English";
#endif
#if UNITY_EDITOR
            if (!_local.ContainsKey(local))
                throw new Exception("Not found Local -> " + local);
#endif
            CurrentLocal = local;
            _currentLocalDictionary = _local[CurrentLocal];
        }

        public void ChangeLanguage(string local)
        {
            if(CurrentLocal == local)return;

            SetLangauge(local);

           _eventSystem.Call(GameShellEvents.ChangeLangauge, new LangaugeParams(local));
        }

        private void ParseCsvFile(string textAsset)
        {
            _local = new Dictionary<string, Dictionary<string, string>>();
            textAsset = textAsset.Replace('\n', '\r');
            var lines = textAsset.Split(new[] { '\r' }, StringSplitOptions.RemoveEmptyEntries);

            var numberLines = lines.Length;
            var numberRows = lines[0].Split(',').Length;

            var associationLocals = new Dictionary<int, string>(numberRows - 1);
         
            var  localId = lines[0].Split(',');
            for (var i = 1; i < numberRows; i++)
            {
                _local.Add(localId[i], new Dictionary<string, string>(numberLines - 1));
                associationLocals.Add(i, localId[i]);
            }

            for (var r = 1; r < numberLines; r++)
            {
                var line = lines[r].Split(',');
                var currentStringId = line[0].ToLower(); 

                foreach (var local in _local)
                {
                    local.Value.Add(currentStringId, null);
                }

                for (var c = 1; c < numberRows; c++)
                {
                    var currentValue = line[c];
                    if (string.IsNullOrEmpty(currentValue)) continue;
                    
                    currentValue = currentValue.Replace("\\n", Environment.NewLine);
                    currentValue = currentValue.Replace("*", ",");
                    currentValue = currentValue.Replace('"', ' ');

                    var key = associationLocals[c];
                    var value = _local[key];
                    value[currentStringId] = currentValue;
                }
            }
        }
     
        public string GetString(string key)
        {
            if (!_currentLocalDictionary.ContainsKey(key))
            {
                throw new Exception("Error wrong key!");
            }

            key = key.ToLower();
            var value = _currentLocalDictionary[key] ?? "id: " + key;
            return value;
        }
    }
}