﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace GameShell.Helpers
{
    [Serializable]
    public struct ButtonItem
    {
        public int Number;
        public Button Button;

        public ButtonItem(int number, Button button)
        {
            Number = number;
            Button = button;
        }
    }

    public class ParentControlHelper : MonoBehaviour
    {
        public Text InputText;
        public Text HeaderText;
        public Text DescriptionText;
        public Text TargetNumberText;
        public List<ButtonItem> Buttons;
    }
}