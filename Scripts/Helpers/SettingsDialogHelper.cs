﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace GameShell.Helpers
{
    [Serializable]
    public struct LangaugeItem
    {
        public string LangaugeId;
        public Toggle Toggle;

        public LangaugeItem(string langaugeId, Toggle toggle)
        {
            LangaugeId = langaugeId;
            Toggle = toggle;
        }
    }

    public class SettingsDialogHelper : MonoBehaviour
    {
        public Text HeaderLangaugeText;
        public Text HeaderMusicText;
        public Text HeaderSoundText;
        public Slider MusicSlider;
        public Slider SoundSlider;

        public List<LangaugeItem> Langauges;
    }
}