﻿using System;
using System.IO;
using UnityEditor;
using UnityEngine;

namespace GameShell.Editor
{
    public class Menu
    {

        [MenuItem("GameShell/Create Default Prefabs", false, 601)]
        static void CreateDefaultPrefab()
        {
            if (!Directory.Exists(Application.dataPath + "/GameShell/"))
            {
                Debug.LogError("Folder structure incompatible, are you already using original folder structure, or have you manually changed the folder structure?");
                return;
            }            

            if (!Directory.Exists(Application.dataPath + "/Resources/"))
                AssetDatabase.CreateFolder("Assets", "Resources");

            if (!Directory.Exists(Application.dataPath + "/Resources/GameShell"))
                AssetDatabase.CreateFolder("Assets/Resources", "GameShell");
            else
                AssetDatabase.DeleteAsset("Assets/Resources/GameShell");

            if (!Directory.Exists(Application.dataPath + "/Resources/GameShell/Data"))
                AssetDatabase.CreateFolder("Assets/Resources/GameShell", "Data");

            if (!Directory.Exists(Application.dataPath + "/Resources/GameShell/Prefabs"))
                AssetDatabase.CreateFolder("Assets/Resources/GameShell", "Prefabs");

            if (!Directory.Exists(Application.dataPath + "/Resources/GameShell/Prefabs/Buttons"))
                AssetDatabase.CreateFolder("Assets/Resources/GameShell/Prefabs", "Buttons");

            if (!Directory.Exists(Application.dataPath + "/Resources/GameShell/Prefabs/Dialogs"))
                AssetDatabase.CreateFolder("Assets/Resources/GameShell/Prefabs", "Dialogs");

            if (!Directory.Exists(Application.dataPath + "/Resources/GameShell/Prefabs/Screens"))
                AssetDatabase.CreateFolder("Assets/Resources/GameShell/Prefabs", "Screens");
            
            AssetDatabase.CopyAsset("Assets/GameShell/Prefabs/GameShell.prefab", "Assets/Resources/GameShell/Prefabs/GameShell.prefab");
            AssetDatabase.Refresh();

            //BUTTONS
            AssetDatabase.CopyAsset("Assets/GameShell/Prefabs/Buttons/ButtonAd.prefab", "Assets/Resources/GameShell/Prefabs/Buttons/ButtonAd.prefab");
            AssetDatabase.CopyAsset("Assets/GameShell/Prefabs/Buttons/ButtonBack.prefab", "Assets/Resources/GameShell/Prefabs/Buttons/ButtonBack.prefab");
            AssetDatabase.CopyAsset("Assets/GameShell/Prefabs/Buttons/ButtonClose.prefab", "Assets/Resources/GameShell/Prefabs/Buttons/ButtonClose.prefab");
            AssetDatabase.CopyAsset("Assets/GameShell/Prefabs/Buttons/ButtonForward.prefab", "Assets/Resources/GameShell/Prefabs/Buttons/ButtonForward.prefab");
            AssetDatabase.CopyAsset("Assets/GameShell/Prefabs/Buttons/ButtonHelp.prefab", "Assets/Resources/GameShell/Prefabs/Buttons/ButtonHelp.prefab");
            AssetDatabase.CopyAsset("Assets/GameShell/Prefabs/Buttons/ButtonHome.prefab", "Assets/Resources/GameShell/Prefabs/Buttons/ButtonHome.prefab");
            AssetDatabase.CopyAsset("Assets/GameShell/Prefabs/Buttons/ButtonLock.prefab", "Assets/Resources/GameShell/Prefabs/Buttons/ButtonLock.prefab");
            AssetDatabase.CopyAsset("Assets/GameShell/Prefabs/Buttons/ButtonPlay.prefab", "Assets/Resources/GameShell/Prefabs/Buttons/ButtonPlay.prefab");
            AssetDatabase.CopyAsset("Assets/GameShell/Prefabs/Buttons/ButtonSettings.prefab", "Assets/Resources/GameShell/Prefabs/Buttons/ButtonSettings.prefab");
            AssetDatabase.Refresh();

            //DIALOGS
            AssetDatabase.CopyAsset("Assets/GameShell/Prefabs/Dialogs/Blocker.prefab", "Assets/Resources/GameShell/Prefabs/Dialogs/Blocker.prefab");
            AssetDatabase.CopyAsset("Assets/GameShell/Prefabs/Dialogs/HelpDialog.prefab", "Assets/Resources/GameShell/Prefabs/Dialogs/HelpDialog.prefab");
            AssetDatabase.CopyAsset("Assets/GameShell/Prefabs/Dialogs/ParentControlDialog.prefab", "Assets/Resources/GameShell/Prefabs/Dialogs/ParentControlDialog.prefab");
            AssetDatabase.CopyAsset("Assets/GameShell/Prefabs/Dialogs/SettingsDialog.prefab", "Assets/Resources/GameShell/Prefabs/Dialogs/SettingsDialog.prefab");
            AssetDatabase.Refresh();

            //SCREENS
            AssetDatabase.CopyAsset("Assets/GameShell/Prefabs/Screens/BackgroundMenu.prefab", "Assets/Resources/GameShell/Prefabs/Screens/BackgroundMenu.prefab");
            AssetDatabase.CopyAsset("Assets/GameShell/Prefabs/Screens/ControlPanel.prefab", "Assets/Resources/GameShell/Prefabs/Screens/ControlPanel.prefab");
            AssetDatabase.CopyAsset("Assets/GameShell/Prefabs/Screens/LoaderScreen.prefab", "Assets/Resources/GameShell/Prefabs/Screens/LoaderScreen.prefab");
            AssetDatabase.CopyAsset("Assets/GameShell/Prefabs/Screens/MainMenu.prefab", "Assets/Resources/GameShell/Prefabs/Screens/MainMenu.prefab");
            AssetDatabase.CopyAsset("Assets/GameShell/Prefabs/Screens/Splash.prefab", "Assets/Resources/GameShell/Prefabs/Screens/Splash.prefab");
            AssetDatabase.Refresh();
            
            //DATA
            AssetDatabase.CopyAsset("Assets/GameShell/Data/LocalizationData.csv", "Assets/Resources/GameShell/Data/LocalizationData.csv");
            AssetDatabase.CopyAsset("Assets/GameShell/Data/SettingsGameShell.asset", "Assets/Resources/GameShell/Data/SettingsGameShell.asset");
            AssetDatabase.Refresh();

            Debug.Log("Succes complete create default settings");
        }

        [MenuItem("GameShell/Add GameShell Container in Scene", false, 602)]
        public static void AddGameShellContainer()
        {
            var source = AssetDatabase.LoadAssetAtPath("Assets/Resources/GameShell/Prefabs/GameShell.prefab", typeof(GameObject));
            if (source == null)
                throw new Exception("Not found GameShell.prefab in directory Assets/Resources/GameShell/Prefabs/");

            GameObject go = PrefabUtility.InstantiatePrefab(AssetDatabase.LoadAssetAtPath("Assets/Resources/GameShell/Prefabs/GameShell.prefab", typeof(GameObject))) as GameObject;
            go.name = "GameShell";
            Selection.activeObject = go;
            Debug.Log("Succes add GameShell container in current scene");
        }

        [MenuItem("GameShell/Delete GameShell from Resource", false, 603)]
        public static void DeleteGameShell()
        {
            AssetDatabase.DeleteAsset("Assets/Resources/GameShell");
            AssetDatabase.Refresh();

            Debug.Log("Succes delete GameShell from Resource");
        }

        [MenuItem("GameShell/Create New Settings")]
        public static void CreateSettings()
        {
            CreateAsset<SettingsGameShell>("GameShell/Data/");
            Debug.Log("Succes created Settings.asset");
        }

        [MenuItem("GameShell/Show Settings")]
        public static void ShowSettings()
        {
            var asset = Resources.Load<SettingsGameShell>("GameShell/Data/SettingsGameShell");
            EditorUtility.FocusProjectWindow();
            Selection.activeObject = asset;
        }    

        public static void CreateAsset<T>(string path) where T : ScriptableObject
        {
            var asset = ScriptableObject.CreateInstance<T>();
        
            var savePath = Path.Combine("Assets/Resources", path);
            var fullPath = Path.Combine(Application.dataPath + "/Resources", path);
        
            if (Directory.Exists(fullPath) == false)
            {
                Directory.CreateDirectory(fullPath);
            }

            string assetPathAndName = AssetDatabase.GenerateUniqueAssetPath(savePath + "/" + typeof(T).Name + ".asset");

            AssetDatabase.CreateAsset(asset, assetPathAndName);

            AssetDatabase.SaveAssets();
            AssetDatabase.Refresh();

            EditorUtility.FocusProjectWindow();
            Selection.activeObject = asset;
        }
    }
}
