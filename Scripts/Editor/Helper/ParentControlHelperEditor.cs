﻿using System.Collections.Generic;
using GameShell.Helpers;
using UnityEditor;
using UnityEngine;
using UnityEngine.UI;

namespace GameShell.Editor
{
    [CustomEditor(typeof(ParentControlHelper))]
    [CanEditMultipleObjects]
    public class ParentControlHelperEditor : UnityEditor.Editor
    {
        private ParentControlHelper _helper;

        void OnEnable()
        {
        }

        public override void OnInspectorGUI()
        {
            _helper = this.target as ParentControlHelper;
            if (_helper == null) return;

            serializedObject.Update();

            _helper.HeaderText = (Text)EditorGUILayout.ObjectField("Header:", _helper.HeaderText, typeof(Text), true);
            _helper.DescriptionText = (Text)EditorGUILayout.ObjectField("Description:", _helper.DescriptionText, typeof(Text), true);
            _helper.TargetNumberText = (Text)EditorGUILayout.ObjectField("Target Number:", _helper.TargetNumberText, typeof(Text), true);
            _helper.InputText = (Text)EditorGUILayout.ObjectField("Input:", _helper.InputText, typeof(Text), true);

            EditorGUILayout.Space();

            GUILayout.BeginHorizontal();
            if (GUILayout.Button("Add"))
                AddElement();

            if (GUILayout.Button("ClearAll"))
                ClearAll();

            GUILayout.EndHorizontal();

            EditorGUILayout.Space();
            
            var removeList = new List<ButtonItem>();
            for (var j = 0; j < _helper.Buttons.Count; j++)
            {
                var associationsItem = _helper.Buttons[j];
                
                GUILayout.BeginHorizontal();
                var togle = (associationsItem.Button != null) ? associationsItem.Button.name : "null";

                EditorGUILayout.LabelField("   Langauge: [" + associationsItem.Number + "]  Button (" + togle + ")");

                if (GUILayout.Button("X", GUI.skin.label, new GUILayoutOption[]
                        {
                            GUILayout.Width(16),
                            GUILayout.Height(16)
                        }))
                {
                    removeList.Add(associationsItem);
                }
                GUILayout.EndHorizontal();
                
                GUILayout.BeginHorizontal();
                GUILayout.Label("", GUILayout.Width(21));

                associationsItem.Number = EditorGUILayout.IntField("Number:", associationsItem.Number);
                  

                GUILayout.EndHorizontal();

                GUILayout.BeginHorizontal();
                GUILayout.Label("", GUILayout.Width(21));

                associationsItem.Button = (Button)EditorGUILayout.ObjectField("Button:", associationsItem.Button, typeof(Button), true);
                GUILayout.EndHorizontal();

                _helper.Buttons[j] = associationsItem;
            }

            foreach (var item in removeList)
            {
                _helper.Buttons.Remove(item);
            }

            removeList.Clear();

            serializedObject.ApplyModifiedProperties();
        }



        private void AddElement()
        {
            _helper.Buttons.Add(new ButtonItem(0, null));
        }

        private void ClearAll()
        {
            _helper.Buttons.Clear();
        }
    }
}