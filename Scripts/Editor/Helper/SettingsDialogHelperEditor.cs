﻿using System.Collections.Generic;
using GameShell.Helpers;
using UnityEditor;
using UnityEngine;
using UnityEngine.UI;

namespace GameShell.Editor
{
    [CustomEditor(typeof(SettingsDialogHelper))]
    [CanEditMultipleObjects]
    public class SettingsDialogHelperEditor : UnityEditor.Editor
    {
        private SettingsDialogHelper _helper;

        void OnEnable()
        {
        }

        public override void OnInspectorGUI()
        {
            _helper = this.target as SettingsDialogHelper;
            if (_helper == null) return;

            serializedObject.Update();

            _helper.HeaderLangaugeText = (Text)EditorGUILayout.ObjectField("Header Langauge:", _helper.HeaderLangaugeText, typeof(Text), true);
            _helper.HeaderMusicText = (Text)EditorGUILayout.ObjectField("Header music:", _helper.HeaderMusicText, typeof(Text), true);
            _helper.MusicSlider = (Slider)EditorGUILayout.ObjectField("Slider music:", _helper.MusicSlider, typeof(Slider), true);

            _helper.HeaderSoundText = (Text)EditorGUILayout.ObjectField("Header sound:", _helper.HeaderSoundText, typeof(Text), true);
            _helper.SoundSlider = (Slider)EditorGUILayout.ObjectField("Slider sound:", _helper.SoundSlider, typeof(Slider), true);

            EditorGUILayout.Space();

            GUILayout.BeginHorizontal();
            if (GUILayout.Button("Add Langauge"))
                AddElement();

            if (GUILayout.Button("ClearAll"))
                ClearAll();

            GUILayout.EndHorizontal();

            EditorGUILayout.Space();
            
            var removeList = new List<LangaugeItem>();
            for (var j = 0; j < _helper.Langauges.Count; j++)
            {
                var associationsItem = _helper.Langauges[j];
                
                GUILayout.BeginHorizontal();
                var togle = (associationsItem.Toggle != null) ? associationsItem.Toggle.name : "null";

                EditorGUILayout.LabelField("   Langauge: [" + associationsItem.LangaugeId + "]  Toggle (" + togle + ")");

                if (GUILayout.Button("X", GUI.skin.label, new GUILayoutOption[]
                        {
                            GUILayout.Width(16),
                            GUILayout.Height(16)
                        }))
                {
                    removeList.Add(associationsItem);
                }
                GUILayout.EndHorizontal();
                
                GUILayout.BeginHorizontal();
                GUILayout.Label("", GUILayout.Width(21));

                associationsItem.LangaugeId = EditorGUILayout.TextField("Id:", associationsItem.LangaugeId);
                  

                GUILayout.EndHorizontal();

                GUILayout.BeginHorizontal();
                GUILayout.Label("", GUILayout.Width(21));

                associationsItem.Toggle = (Toggle)EditorGUILayout.ObjectField("Toggle:", associationsItem.Toggle, typeof(Toggle), true);
                GUILayout.EndHorizontal();

                _helper.Langauges[j] = associationsItem;
            }

            foreach (var item in removeList)
            {
                _helper.Langauges.Remove(item);
            }

            removeList.Clear();

            serializedObject.ApplyModifiedProperties();
        }



        private void AddElement()
        {
            _helper.Langauges.Add(new LangaugeItem("none", null));
        }

        private void ClearAll()
        {
            _helper.Langauges.Clear();
        }
    }
}