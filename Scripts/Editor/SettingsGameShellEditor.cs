﻿using System.IO;
using System.Linq;
using UnityEditor;
using UnityEngine;

namespace GameShell.Editor
{// [CanEditMultipleObjects]
    [CustomEditor(typeof(SettingsGameShell))]
    public class SettingsGameShellEditor : UnityEditor.Editor
    {
       // private SettingsGameShell settings;

        void OnEnable()
        {
        }

        public override void OnInspectorGUI()
        {
            var settings = target as SettingsGameShell;
            if (settings == null) return;

           // serializedObject.UpdateIfDirtyOrScript();
           
            GUILayout.Box("CONFIGURATION", GUILayout.MaxWidth(Screen.width));
            Separator();

            GUILayout.BeginVertical("LOCALIZATION", "box");
            GUILayout.Space(20);

            settings.EnableLocalization = GUILayout.Toggle(settings.EnableLocalization, " enable");

            if (settings.EnableLocalization)
            {
                var path = (string.IsNullOrEmpty(settings.PathToLocalizationFile)) ? "none": settings.PathToLocalizationFile;
                EditorGUILayout.LabelField("Path csv file:", path);

                if (GUILayout.Button("Set path"))
                {
                    var choosenPath = EditorUtility.OpenFilePanel("Select localization CSV file", Application.dataPath, "csv");
                    if (choosenPath.Length == 0) return;
                     choosenPath = Path.ChangeExtension(choosenPath, "");
                   
                    var resultPath = choosenPath.Split('/').ToList();

                    bool enable = false;
                    string result = "";
                    foreach (string dir in resultPath)
                    {
                        if (enable)
                            result += dir + "/";

                        if (dir == "Resources")
                            enable = true;
                    }
                    result = result.Remove(result.IndexOf('.'), 2);
                    settings.PathToLocalizationFile = result;
                }
            }

            GUILayout.Space(10);
            GUILayout.EndVertical();

            GUILayout.BeginVertical("SPLASH SCREEN", "box");
            GUILayout.Space(20);

            settings.SplashScreen = (GameObject)EditorGUILayout.ObjectField("Splash prefab:", settings.SplashScreen, typeof(GameObject), true);
            settings.TimeSplashComplete = EditorGUILayout.FloatField("Time complete:", settings.TimeSplashComplete);
            settings.AnimationSplashComplete = GUILayout.Toggle(settings.AnimationSplashComplete, " animation complete");

            GUILayout.Space(10);
            GUILayout.EndVertical();

            GUILayout.BeginVertical("MAIN MENU SCREEN", "box");
            GUILayout.Space(20);

            settings.MainMenu = (GameObject)EditorGUILayout.ObjectField("Screen:", settings.MainMenu, typeof(GameObject), true);
            settings.BackgroundMenu = (GameObject)EditorGUILayout.ObjectField("Background:", settings.BackgroundMenu, typeof(GameObject), true);

            GUILayout.Space(10);
            EditorGUILayout.LabelField("BUTTONS");
            Separator();

            settings.ButtonPlay = (GameObject)EditorGUILayout.ObjectField("Play:", settings.ButtonPlay, typeof(GameObject), true);
            settings.ButtonSettings = (GameObject)EditorGUILayout.ObjectField("Settings:", settings.ButtonSettings, typeof(GameObject), true);
            settings.ButtonAd = (GameObject)EditorGUILayout.ObjectField("Ad:", settings.ButtonAd, typeof(GameObject), true);
            settings.ButtonHelp = (GameObject)EditorGUILayout.ObjectField("Help:", settings.ButtonHelp, typeof(GameObject), true);

            GUILayout.Space(10);
            GUILayout.EndVertical();

            GUILayout.BeginVertical("CONTROL GAME SCREEN", "box");
            GUILayout.Space(20);

            settings.ControlPanel = (GameObject)EditorGUILayout.ObjectField("Control panel:", settings.ControlPanel, typeof(GameObject), true);
            settings.EnableArrowButtons = GUILayout.Toggle(settings.EnableArrowButtons, " enable arrow buttons");

            GUILayout.Space(10);
            EditorGUILayout.LabelField("BUTTONS");
            Separator();

            settings.ButtonForward = (GameObject)EditorGUILayout.ObjectField("Formard:", settings.ButtonForward, typeof(GameObject), true);
            settings.ButtonBack = (GameObject)EditorGUILayout.ObjectField("Back:", settings.ButtonBack, typeof(GameObject), true);
          
            GUILayout.Space(10);
            GUILayout.EndVertical();

            GUILayout.BeginVertical("COMMON BUTTONS", "box");
            GUILayout.Space(20);
            settings.TimeDeactivationButtons = EditorGUILayout.FloatField("Time deactivation:", settings.TimeDeactivationButtons);
            settings.ButtonHome = (GameObject)EditorGUILayout.ObjectField("Home:", settings.ButtonHome, typeof(GameObject), true);
            settings.ButtonLock = (GameObject)EditorGUILayout.ObjectField("Lock:", settings.ButtonLock, typeof(GameObject), true);
            settings.ButtonClose = (GameObject)EditorGUILayout.ObjectField("Close:", settings.ButtonClose, typeof(GameObject), true);
         
            GUILayout.Space(10);
            GUILayout.EndVertical();

            GUILayout.BeginVertical("COMMON DIALOGS", "box");
            GUILayout.Space(20);

            settings.SettingsDialog = (GameObject)EditorGUILayout.ObjectField("Settings:", settings.SettingsDialog, typeof(GameObject), true);
            settings.HelpDialog = (GameObject)EditorGUILayout.ObjectField("Help:", settings.HelpDialog, typeof(GameObject), true);
            settings.ParentControlDialog = (GameObject)EditorGUILayout.ObjectField("ParentControl:", settings.ParentControlDialog, typeof(GameObject), true);
            settings.BlockerDialog = (GameObject)EditorGUILayout.ObjectField("Blocker:", settings.BlockerDialog, typeof(GameObject), true);

            GUILayout.Space(10);
            GUILayout.EndVertical();

            //DrawDefaultInspector();
            if (GUI.changed)
            {
                EditorUtility.SetDirty(settings);
            }
        }

        public void Separator()
        {
            GUI.backgroundColor = Color.black;
            GUILayout.Box("", GUILayout.MaxWidth(Screen.width), GUILayout.Height(2));
            GUI.backgroundColor = Color.white;
        }

    }
}