﻿using System;
using UnityEngine;

namespace GameShell
{
    [Serializable]
    public class SettingsGameShell:ScriptableObject
    {
        public GameObject SplashScreen;
        public float TimeSplashComplete = 0f;
        public float TimeDeactivationButtons = 0f;
        public bool AnimationSplashComplete = true;

        public GameObject BackgroundMenu;
        public GameObject MainMenu;

        public GameObject ButtonPlay;
        public GameObject ButtonSettings;
        public GameObject ButtonAd;
        public GameObject ButtonHelp;
        public GameObject ButtonForward;
        public GameObject ButtonBack;

        public GameObject ButtonHome;
        public GameObject ButtonLock;
        public GameObject ButtonClose;

        public GameObject ControlPanel;
        public bool EnableArrowButtons = true;

        public GameObject SettingsDialog;
        public GameObject HelpDialog;
        public GameObject ParentControlDialog;
        public GameObject BlockerDialog;

        public string PathToLocalizationFile;
        public bool EnableLocalization = true;
    }
}
