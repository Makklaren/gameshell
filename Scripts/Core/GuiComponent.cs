﻿using GameShell.Core.Extensions;
using UnityEngine;

namespace GameShell.Core
{
    public abstract class GuiComponent
    {
        protected GameObject Parent;
        public GameObject View;
        public GameObject OriginalView;
        public Transform Transform;
        public RectTransform RectTransform;

        protected GuiComponent(GameObject parent, GameObject originalView)
        {
            Parent = parent;
            OriginalView = originalView;
        }

        public void Init()
        {
            View = Object.Instantiate(OriginalView);
            View.name = OriginalView.name;
            Parent.AddChildUI(View);
            Transform = View.transform;
            RectTransform = View.GetComponent<RectTransform>();

            OnInit();
        }

        public virtual void Show()
        {
            View.SetActive(true);
        }

        public virtual void Hide()
        {
            View.SetActive(false);
        }

        public void Update()
        {
            OnUpdate();
        }

        public void Destroy()
        {
            OnDestroy();

            Object.Destroy(View);

            Transform.SetParent(null);
            Transform = null;
            RectTransform = null;
            View = null;
            Parent = null;
        }

        protected abstract void OnInit();
        protected abstract void OnDestroy();
        protected abstract void OnUpdate();
    }
}
