﻿namespace GameShell.Core.EventSystem
{
    public class Event
    {
        public string Name;
        public Event(string name)
        {
            Name = name;
        }
    }
}
