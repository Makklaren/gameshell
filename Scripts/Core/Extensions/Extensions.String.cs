﻿namespace GameShell.Core.Extensions
{
    public static partial class Extension
    {
        public static string ToLocalization( this string stringId)
        {
            return (GameShell.Instance.Settings.EnableLocalization)? GameShell.Instance.Localization.GetString(stringId) : stringId;
        }
    }
}